# Ty a Ja

## Staging app

https://ty-a-ja-staging.herokuapp.com/

## Descriptionn

## Running the application locally

### Prerequisities

**Ruby**  - You can install rvm - see https://rvm.io/rvm/install for details

**Postgres**  - For innstructios how to install postgresql on a Macbook see https://www.moncefbelyamani.com/how-to-install-postgresql-on-a-mac-with-homebrew-and-lunchy/
 - user `root` with blank password is expected, you can either create this user (after installing postgres) by running `createuser -P -s -e root` or change the cofiguration in `config/database.yml`

 **Imagemagick** - On Mac you can install it using homebrew `brew install imagemagick`

 ## Running the application

 ### Database

Creating  database
 `bundle exec rake db:create`

 Running migrations
 `bundle exec rake db:migrate`

 Seed data
 `bundle exec rake db:seed`

 ### Runnig the local server

`bundle update` for installing and updatig the gems
 `rails s` for runnning the server

 ### Running tests

 `bundle exec rspec spec`
