# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Post.destroy_all
Event.destroy_all
User.destroy_all
Sponsor.destroy_all

user = User.create(email: "evusidlo@gmail.com", password: "genius")
User.create(email: "elisak@gmail.com", password: "supercute")
User.create(email: "jajulak@gmail.com", password: "rubymaster")

17.times do
  User.create(email: Faker::Internet.email, password: Faker::Internet.password)
end

5.times do
  Event.create(title: Faker::DrWho.character, description: Faker::Lorem.paragraphs(6).join("<br/>"),
    date: Faker::Date.between(1.year.ago, 1.year.from_now), event_type: :hobby_group)
end

10.times do
  Event.create(title: Faker::DrWho.character, description: Faker::Lorem.paragraphs(6).join("<br/>"),
    date: Faker::Date.between(1.year.ago, 1.year.from_now), event_type: :planned)
end

5.times do
  Event.create(title: Faker::DrWho.character, description: Faker::Lorem.paragraphs(6).join("<br/>"),
    date: Faker::Date.between(1.year.ago, 1.year.from_now), event_type: :course)
end

Event.create(title: 'Festival nna konci léta', description: Faker::Lorem.paragraphs(6).join("<br/>"),
    date: Faker::Date.between(1.year.ago, 1.year.from_now), event_type: :festival)

20.times do
  user.posts.create!(title: Faker::ChuckNorris.fact, text: Faker::SiliconValley.quote)
end

20.times do
  Sponsor.create(name: Faker::LordOfTheRings.character, web: Faker::Internet.url, logo: open(Faker::Avatar.image))
end
