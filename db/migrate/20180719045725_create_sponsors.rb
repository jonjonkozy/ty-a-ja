class CreateSponsors < ActiveRecord::Migration[5.2]
  def change
    create_table :sponsors do |t|
      t.string :name, null: false
      t.string :web

      t.timestamps
    end
  end
end
