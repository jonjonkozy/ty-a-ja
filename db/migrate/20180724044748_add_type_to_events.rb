class AddTypeToEvents < ActiveRecord::Migration[5.2]
  def change
    add_column :events, :event_type, :integer, null: false, default: 1
  end
end
