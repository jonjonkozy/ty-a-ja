class AddLogoToSponsors < ActiveRecord::Migration[5.2]
  def change
    add_column :sponsors, :logo, :string
  end
end
