class Photo < ApplicationRecord
  validates :name, :image, presence: true
  mount_uploader :image, PhotoUploader
end
