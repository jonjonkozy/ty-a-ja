class Event < ApplicationRecord
  enum event_type: [:planned, :hobby_group, :course, :festival]

  alias_attribute :type, :event_type

  validates :title, presence: true, length: { minimum: 3 }
  validates :event_type, presence: true
  validate :date_around_today

  TYPE_TITLES = {
    planned: 'Plánované akce',
    hobby_group: 'Kroužky',
    course: 'Kurzy',
    festival: 'Festival na konci léta'
  }.freeze

  def event_type_title
    TYPE_TITLES[event_type.to_sym]
  end

  def date_around_today
    if !date
      errors.add(:date, 'is required')
    elsif date <= 10.year.ago || date >= 2.years.from_now
      errors.add(:date, 'can not be more than 1 year in the past or 2 years in the future')
    end
  end
end
