class Sponsor < ApplicationRecord
  validates :name, presence: true
  validates :web, format: URI.regexp(%w(http https))
  mount_uploader :logo, LogoUploader
end
