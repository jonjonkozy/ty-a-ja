class PhotosController < ApplicationController
  before_action :authenticate_user!, only: [:new, :create, :update]

  def index
    @photos = Photo.all
  end

  def new
    @photo = Photo.new
  end

  def show
    @photo = Photo.find(params[:id])
  end

  def create
    @photo = Photo.create(photo_params)

    if @photo.save
      redirect_to @photo, notice: 'Fotka byla přidána.'
    else
      render action: 'new'
    end
  end

  def destroy
    @photo = Photo.find(params[:id])
    @photo.destroy

    redirect_to photos_path
  end

  private

  def photo_params
    params.require(:photo).permit(:name, :image, :description)
  end
end
