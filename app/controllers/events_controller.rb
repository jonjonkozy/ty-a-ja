class EventsController < ApplicationController
  before_action :authenticate_user!, only: [:new, :create, :edit, :update]

  def index
    check_params!
    @events = Event.all
    @events = @events.where(type: params[:type]) if params[:type]
    @events = @events.order(date: :desc)
  end

  def new
    @event = Event.new
  end

  def edit
    @event = Event.find(params[:id])
  end

  def show
    @event = Event.find(params[:id])
  end

  def create
    @event = Event.new(event_params)

    if @event.save
      redirect_to @event, notice: 'Akce byla vytvořena.'
    else
      render action: 'new'
    end
  end

  def update
    @event = Event.find(params[:id])

    if @event.update(event_params)
      redirect_to @event, notice: 'Akce byla aktualizována.'
    else
      render action: 'edit'
    end
  end

  private

  def event_params
    params.require(:event).permit(:title, :description, :date, :type)
  end

  def check_params!
    return unless params[:type]
    raise ArgumentError unless Event.event_types.keys.include?(params[:type])
  end
end
