class SponsorsController < ApplicationController
  before_action :authenticate_user!, only: [:new, :create, :edit, :update]

  def index
    @sponsors = Sponsor.all
  end

  def new
    @sponsor = Sponsor.new
  end

  def edit
    @sponsor = Sponsor.find(params[:id])
  end

  def show
    @sponsor = Sponsor.find(params[:id])
  end

  def create
    @sponsor = Sponsor.create(sponsor_params)

    if @sponsor.save
      redirect_to @sponsor, notice: 'Sponzor byl vytvořen.'
    else
      render action: 'new'
    end
  end

  def update
    @sponsor = Sponsor.find(params[:id])

    if @sponsor.update(sponsor_params)
      redirect_to @sponsor, notice: 'Sponzor byl aktualizován.'
    else
      render action: 'edit'
    end
  end

  private

  def sponsor_params
    params.require(:sponsor).permit(:name, :web, :logo)
  end
end
