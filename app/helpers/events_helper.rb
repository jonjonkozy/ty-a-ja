module EventsHelper
  def types_options
    { '' => nil }.merge(Event::TYPE_TITLES.invert)
  end
end
