module ApplicationHelper
  def page_title
    'Ty a Já'
  end

  def page_description
    'Občaské sdružeí Ty a Já'
  end

  def format_date(date)
    return '' unless date

    date.strftime('%-d.%-m.%Y')
  end
end
