Rails.application.routes.draw do

  get 'sites/jak_prispet'
  get 'sites/o_nas'
  get 'sites/kontakty'
  devise_for :users
  resources :events
  resources :posts
  resources :users
  resources :sponsors
  resources :photos

  get 'home/index'

  root 'home#index'
end
