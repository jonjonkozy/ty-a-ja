FactoryBot.define do
  factory :sponsor do
    name Faker::LordOfTheRings.character
    web Faker::Internet.url
  end
end
