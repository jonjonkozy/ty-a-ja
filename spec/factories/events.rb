FactoryBot.define do
  factory :event do
    title Faker::HarryPotter.location
    description Faker::ChuckNorris.fact
    date Faker::Date.between(2.weeks.ago, 2.weeks.from_now)
    event_type :planned
  end
end
