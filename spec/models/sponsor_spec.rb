require 'spec_helper'

RSpec.describe Sponsor, type: :model do
  describe 'validations' do
    it { is_expected.to validate_presence_of :name }
    it { is_expected.to allow_value('https://gitlab.com/').for(:web) }
    it { is_expected.not_to allow_value('evca vladne').for(:web) }
  end
end
