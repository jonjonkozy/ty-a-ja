require 'spec_helper'

RSpec.describe Event, type: :model do
  let(:valid_params) do
    {
      title: 'some title',
      date: Date.today,
      event_type: :planned
    }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of :title }

    it 'is valid with valid params' do
      expect(Event.new(valid_params)).to be_valid
    end

    context 'date' do
      it 'is invalid without date' do
        valid_params.delete(:date)

        expect(Event.new(valid_params)).not_to be_valid
      end

      it 'is invalid with date too far in the future' do
        valid_params[:date] = 25.years.from_now

        expect(Event.new(valid_params)).not_to be_valid
      end

      it 'is invalid with date too far in the past' do
        valid_params[:date] = 25.years.ago

        expect(Event.new(valid_params)).not_to be_valid
      end
    end
  end

  context 'event types' do
    context 'validations' do
      it 'accepts types specified by enum array' do
        expect(Event.new(valid_params)).to be_valid
      end

      it 'does not accept random event type' do
        invalid_params = {
          title: 'some title',
          date: Date.today,
          event_type: :grgrgr
        }

        expect { Event.new(invalid_params) }.to raise_error(ArgumentError)
      end
    end

    context '#event_type_title' do
      it 'returns a correct title for the event' do
        event = create(:event, event_type: :hobby_group)

        expect(event.event_type_title).to eq('Kroužky')
      end
    end
  end
end
