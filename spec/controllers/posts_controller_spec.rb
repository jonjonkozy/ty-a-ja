require 'spec_helper'

describe PostsController, type: :controller do
  let(:user) { create(:user) }

  shared_examples 'user authentication' do |expected_ok_status|
    context 'when a user is not logged in' do
      it 'redirects to sign in page' do
        subject

        expect(response).to redirect_to(new_user_session_url)
      end
    end

    context 'when a user is signed in' do
      before do
        sign_in(user)

        subject
      end

      it 'renders correct status' do
        expect(response.status).to eq(expected_ok_status)
      end

      it 'does not redirect to the sign in page' do
        expect(response).not_to redirect_to(new_user_session_url)
      end
    end
  end

  describe '#new' do
    subject { get :new }

    it_behaves_like 'user authentication', 200
  end

  describe '#create' do
    let(:params) do
      {
        post: {
          title: 'some_title',
          text: 'some text'
        }
      }
    end

    subject { post :create, params: params }

    it_behaves_like 'user authentication', 302

    context 'when a user is signed in' do
      before do
        sign_in(user)
      end

      context 'when all parameters are correct' do
        it 'creates a new post' do
          expect { subject }.to change { Post.count }.from(0).to(1)
        end

        it 'redirects to the new post page' do
          subject

          expect(response).to redirect_to(post_url(Post.last))
        end
      end

      context 'when post param is missing' do
        let(:params) { { not_valid_key: 'something' } }

        it 'raises an exception' do
          expect { subject }.to raise_error(ActionController::ParameterMissing)
        end
      end

      context 'when post title param is missing' do
        before do
          params[:post].delete(:title)
        end

        it 'does not create a new post' do
          expect { subject }.not_to change { Post.count }.from(0)
        end

        it 'redirects to the post form' do
          subject

          expect(response).to render_template(:new)
        end
      end
    end
  end
end
