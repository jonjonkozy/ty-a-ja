source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.4.4'

require 'uri'

gem 'carrierwave', '~> 1.2.3'

gem 'mini_magick', '~> 4.8.0'

gem 'devise', '~> 4.4.3'
# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.2.0'
# use postgres
gem 'pg', '~> 0.18.2', group: :postgres
# Use Puma as the app server
gem 'puma', '~> 3.11'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'

# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5'

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.1.0', require: false

# Use haml templatig system
gem 'haml', '~> 5.0'

# Use bootstrap for basic styling
gem 'bootstrap', '~> 4.1.1'

# Use jquery
gem 'jquery-rails'
gem 'jquery-ui-rails'

gem 'faker'

group :development, :test, :ci do
  gem 'pry-rails', '~> 0.3.4'

  gem 'database_cleaner', '~> 1.7'
  gem 'factory_bot_rails', '~> 4.10'
  gem 'rspec-rails', '~> 3.7'
  gem 'rails-controller-testing', '~> 1.0'
  gem 'shoulda-matchers', '~> 3.1.2'
  gem 'rubocop', '~> 0.52.1'
  gem 'rubocop-rspec', '~> 1.22.1'
  gem 'timecop', '~> 0.8.0'
end

group :development do
  # Access an interactive console on exception pages or by calling 'console' anywhere in the code.
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

group :test, :ci do
  # Adds support for Capybara system testing and selenium driver
  gem 'capybara', '>= 2.15', '< 4.0'
  gem 'selenium-webdriver'
  # Easy installation and use of chromedriver to run system tests with Chrome
  gem 'chromedriver-helper'
end

group :ci do
 gem 'platform-api'
end
